var elasticsearch = require('elasticsearch');
var moment = require('moment');

var elasticClient = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'info'
});

var indexName = 'logstash-*';

function getErrors(apikey, starttime, endtime) {
  var statusCode
  var query = "apikey:" + apikey;
  return elasticClient.search({
    index: indexName,
    body: {
      _source: [
        '@timestamp',
        'account',
        'apikey',
        'clientip',
        'country',
        'url',
        'response'
      ],
      "size": 0,
      "query": {
        "bool": {
          "must": [
            {
              "query_string": {
                "query": query,
                "analyze_wildcard": true,
                "default_field": "*"
              }
            },
            {
              "range": {
                "@timestamp": {
                  "gte": starttime,
                  "lte": endtime,
                  "format": "epoch_millis"
                }
              }
            }
          ],
          "filter": [],
          "should": [],
          "must_not": [
            {
              "match_phrase": {
                "response": {
                  "query": 200
                }
              }
            }
          ]
        }
      }
    }
  });
}

function getSuccess(apikey, starttime, endtime) {

  var statusCode
  var query = "apikey:" + apikey + " and response:200";
  console.log("elastic query: " + query);
  return elasticClient.search({
    index: indexName,
    body: {
      _source: [
        '@timestamp',
        'account',
        'apikey',
        'clientip',
        'country',
        'url',
        'response'
      ],
      "size": 0,
      "query": {

        "bool": {
          "must": [
            {
              "query_string": {
                "query": query,
                "analyze_wildcard": true,
                "default_field": "*"
              }
            },
            {
              "range": {
                "@timestamp": {
                  "gte": starttime,
                  "lte": endtime,
                  "format": "epoch_millis"
                }
              }
            }
          ],
          "filter": [],
          "should": [],
          "must_not": []
        }
      }
    }
  });
}

function getRandomResults(number) {
  return elasticClient.search({
    index: indexName,
    body: {
      _source: [
        '@timestamp',
        'account',
        'apikey',
        'clientip',
        'country',
        'url',
        'response'
      ],
      query: {
        function_score: {
          query: {
            match_all: {}
          },
          functions: [
            {
              random_score: {}
            }
          ]
        }
      }
    }
  });
}
exports.getRandomResults = getRandomResults;
exports.getSuccess = getSuccess;
exports.getErrors = getErrors;
